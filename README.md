Aplicativo de listagem de médicos

Após realizar o download do repositorio para rodar em modo de desenvolvimento:

```sh
cd consultadobem
npm install
npm start
```

Para gerar um build do aplicativo:

```sh
cd consultadobem
npm install
npm run build
```
Qualquer dúvida, é só entrar em contato!

Grande abraço
Matheus