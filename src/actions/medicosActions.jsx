import Resources from '../Resources';


export const FETCH_MEDICOS 		 	= 'FETCH_MEDICOS';
export const FETCH_MEDICOS_START 	= 'FETCH_MEDICOS_START';
export const FETCH_MEDICOS_REJECTED = 'FETCH_MEDICOS_REJECTED';


export const loadMedicos = ( ) => {
	return ( dispatch ) => {
		dispatch({type: FETCH_MEDICOS_START});
		return  Resources.get('medicos')
				.then( response => {
					dispatch({type: FETCH_MEDICOS, payload: response});
				})
				.catch( error => {
					dispatch({type: FETCH_MEDICOS_REJECTED, payload: error});
				}); 
	};
}
