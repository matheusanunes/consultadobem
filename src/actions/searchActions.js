
export const SEARCH = 'SEARCH';


export const searchTerm = ( term = '' ) => {
	return ( dispatch ) => {
		dispatch({type: SEARCH, payload: term});
	};
}
