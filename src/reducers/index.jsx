import { combineReducers } from 'redux';

import { medicosReducer as medicos } from './medicosReducer';
import { searchReducer as search } from './searchReducer';

const rootReducer = combineReducers({
    medicos,
    search,
});

export default rootReducer;
