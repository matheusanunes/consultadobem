
import { SEARCH } from '../actions/searchActions';

export function searchReducer ( state = {term: ''}, action ) {

	switch( action.type ) {
		case SEARCH:
			return {...state};

		default:
			return state;
	}

}