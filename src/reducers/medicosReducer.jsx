
import { FETCH_MEDICOS, FETCH_MEDICOS_START } from '../actions/medicosActions';
import { SEARCH }							  from '../actions/searchActions';

export function medicosReducer ( state = {fetched:false, fetching:false, medicos: []}, action ) {

	switch( action.type ) {
		case FETCH_MEDICOS:
			let medicos = action.payload.data;

			return {...state, fetched: true, fetching: false, medicos};

		case FETCH_MEDICOS_START:
			return {...state, fetching: true };

		case SEARCH:
			let filtrados = state.medicos.filter( medico => {
				return (
					medico.nome.toLowerCase().includes( action.payload.toLowerCase() ) ||
					medico.CRM.toLowerCase().includes( action.payload.toLowerCase() ) ||
					medico.cidade_atendimento.toLowerCase().includes( action.payload.toLowerCase() ) ||
					medico.bairro_atendimento.toLowerCase().includes( action.payload.toLowerCase() ) ||
					medico.horarios.filter( hora => hora.toLowerCase().includes( action.payload.toLowerCase() ) ).length > 0 ||
					medico.especialidades.filter( spec => spec.toLowerCase().includes( action.payload.toLowerCase() ) ).length > 0
				);
			});
			return {...state, filtrados };

		default:
			return state;
	}

}