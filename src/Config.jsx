
/**
 * Factory for app configurations
 */

class ConfigClass {

 	configs 	= {
 		// Define o ambiente de trabalho
		// _dev = Desenvolvimento, _prod = Produção
		mode: '_dev',

		// Define o banco de dados da cidade
		cidade_padrao: 'São Paulo',

		// Tempo entre uma sessao e outra
		session_time: 30,

		// Local que o navegador irá gralet os dados do usuário ('localStorage', 'sessionStorage')
		browser_storage: 'localStorage',

		server: {

			_dev: {
				
				// URL base para os APIs
				base_url: 'https://api.myjson.com/bins'
			},
		
			// variaveis de servidor
			_prod: {
				
				// URL base para os APIs
				base_url: 'https://api.myjson.com/bins'
			}
		},

		// Limitar os resultados das paginações a X resultados
		resultados_por_pagina: 10
 	};
 	
 	// Retorna a configuração requisitada
	get = ( prop ) => {
			let context = this;

			return prop.split('.').reduce(function(prev, curr) {
			if( typeof prev[curr] === 'undefined' ){
				if( typeof prev[ context.configs.mode ] !== 'undefined' ) {
					return prev[ context.configs.mode ][curr];
				} else if ( typeof prev.default !== 'undefined' ){
					return prev.default[curr];
				} else if( typeof prev[ context.configs.mode+'_'+curr ] ){
					return prev[ context.configs.mode+'_'+curr ];
				}
			}

			return prev ? prev[curr] : undefined;
		}, this.configs || {});
	};

	// Define ou sobrescreve uma configuração
	set = ( prop ) =>{
		let a = this.configs, path = prop.split('.'), value = '';

		for (let i = 0; i < path.length; i++) {
			let curr = path[i];
			
			if( typeof a[curr] === 'undefined' ){

				if( typeof a[ this.configs.mode ] !== 'undefined' && typeof a[ this.configs.mode ][ curr ] !== 'undefined' ) {
				
					if( i === (path.length - 1) ){
						a[ this.configs.mode][ curr ] = value;
					} else {
						a = a[ this.configs.mode ][ curr ];
					}
				
				} else if( typeof a.default !== 'undefined' && typeof a.default[ curr ] !== 'undefined' ) {

					if( i === (path.length - 1) ){
						a.default[ curr ] = value;
					} else {
						a = a.default[ curr ];
					}

				} else if( typeof a[ this.configs.mode+'_'+curr ] !== 'undefined' ) {

					if( i === (path.length - 1) ){
						a[ this.configs.mode+'_'+curr ] = value;
					} else {
						a = a[ this.configs.mode+'_'+curr ];
					}
				} else {

					if( i === (path.length - 1) ){
						a[curr] = value;
					} else {
						a[curr] = {};
						a = a[curr];
					}

				}
			}else {

				if( i === (path.length - 1) ){
					a[curr] = value;
				} else {
					a = a[curr];
				}
			}
		}

		return value;
	}

}

const Config = new ConfigClass();
export default Config;