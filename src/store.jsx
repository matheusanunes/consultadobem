import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const DevTools = typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? 
                 window.devToolsExtension() : f => f; /*Ligação com o Redux Dev Tools*/

const middleware = applyMiddleware(thunk);

export default createStore(
        rootReducer,
        compose(
            middleware,
            DevTools
        )
    );
