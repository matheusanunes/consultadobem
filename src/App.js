import React, { Component } from 'react';
import { Route } 			from 'react-router-dom';
import { connect } 			from 'react-redux';

import 'semantic-ui-css/semantic.min.css';
import './App.css';

import { Container, Grid, Segment } from 'semantic-ui-react';

import MenuPrincipal 		from './components/MenuPrincipal';
import { Detalhes, Lista } 	from './medicos';

import { loadMedicos } 		from './actions/medicosActions';

class App extends Component {

	componentWillMount() {
		if( !this.props.medicos.fetched && !this.props.medicos.fetching ) {
			this.props.dispatch(loadMedicos());
		}
	}

	render() {
		return (
	 		<div id={this.constructor.name}>
	 			<Container>
	 				<Grid stackable>
	 					<Grid.Row>
	 						<Grid.Column width={4}>
	 							<MenuPrincipal />
	 						</Grid.Column>

	 						<Grid.Column width={12}>
	 							<Segment>
									<Route exact path="/"    			 component={Lista} />
									<Route exact path="/:id" 			 component={Detalhes} />
									<Route exact path="/cidades" 		 component={Detalhes} />
									<Route exact path="/especializacoes" component={Detalhes} />
	 							</Segment>
	 						</Grid.Column>
	 					</Grid.Row>
					</Grid>
	 			</Container>
			</div>
		);
	}
}


const mapStateToProps = ( store ) => {
	return {
		medicos: store.medicos
	};
}

export default connect(mapStateToProps)(App);
