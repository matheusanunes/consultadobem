import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { Dimmer, Loader, Table } from 'semantic-ui-react';

class ListaComponent extends Component {

	state = { redir: false, crm: null };

	handleClick = ( e, crm ) => {
		let redir = true;
		this.setState({ redir, crm });
	}

	populateRows = () => {

		
		if( this.props.medicos.fetched && this.props.medicos.medicos.length > 0 ) {
			let rows = [],
				medicos = this.props.medicos.filtrados ? this.props.medicos.filtrados : this.props.medicos.medicos;

			medicos.map( ({nome, CRM, cidade_atendimento, bairro_atendimento}, index) => rows.push(
				<Table.Row key={index} onClick={(e) => this.handleClick(e, CRM)}>
					<Table.Cell>{ nome }</Table.Cell>
					<Table.Cell>{ CRM }</Table.Cell>
					<Table.Cell>{ cidade_atendimento }</Table.Cell>
					<Table.Cell>{ bairro_atendimento }</Table.Cell>
				</Table.Row>)
			);

			return rows;
		}

		return <Table.Row>
					<Table.Cell colSpan={4}>Nenhum médico encontrado ;(</Table.Cell>
				</Table.Row>;
	}

	render() {
		if( this.state.redir ) {
			return <Redirect to={`/${this.state.crm}`} />
		}

		const rows = this.populateRows();

		return <div id={this.constructor.name}>
			<Table basic='very' striped selectable>
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>Nome</Table.HeaderCell>
						<Table.HeaderCell>CRM</Table.HeaderCell>
						<Table.HeaderCell>Cidade</Table.HeaderCell>
						<Table.HeaderCell>Bairro</Table.HeaderCell>
					</Table.Row>
				</Table.Header>

				<Table.Body>
					{ rows }
				</Table.Body>
			</Table>

			<Dimmer inverted active={this.props.medicos.fetching}>
				<Loader inverted>Carregando...</Loader>
			</Dimmer>
		</div>;
	}
}

const mapStateToProps = ( store ) => {
	return {
		medicos: store.medicos
	};
}

export default connect(mapStateToProps)(ListaComponent);