import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { Button, Dimmer, Divider, Grid, Header, List, Loader, Icon } from 'semantic-ui-react';

class DetalhesComponent extends Component {

	state = { medico: {}, redir: false };

	componentWillMount() {
		this.setMedic( this.props );
	}

	componentWillReceiveProps( props ) {
		this.setMedic( props );
	}

	setMedic = ( props ) => {
		if( props.medicos.fetched && props.medicos.medicos.length > 0 ) {
			let crm 	= props.match.params.id,
				medico 	= props.medicos.medicos.filter( med => med.CRM === crm )[0];

			this.setState({ medico });
		}
	}

	back = () => {
		let redir = true;
		this.setState({ redir });
	}

	render() {
		if( this.state.redir ) {
			return <Redirect to='/' />;
		}


		let { nome, CRM, cidade_atendimento, bairro_atendimento, horarios, especialidades } = this.state.medico;
		let specs = [];
		
		if( especialidades !== undefined && especialidades.length > 0 ) {
			specs = especialidades.map( espc => { return <List.Item key={espc}>{espc}</List.Item> });
		}

		return <div id={this.constructor.name}>
			<Button onClick={ this.back }><Icon name='arrow left' /> Voltar</Button>

			<Header as='h2' icon textAlign='center'>
				<Icon name='user' circular />
				<Header.Content>
					{ nome }
				</Header.Content>
				<Header.Subheader>
					CRM: { CRM }
				</Header.Subheader>
			</Header>

			<Divider />

			<Grid padded>
				<Grid.Row columns={3}>
					<Grid.Column>
						<Header as='h3' color='grey'>
							<Icon name='marker' /> 
							<Header.Content>
								Endereço
							</Header.Content>
						</Header>
						<p>{ `${bairro_atendimento} - ${cidade_atendimento}`}</p>
					</Grid.Column>

					<Grid.Column>
						<Header as='h3' color='grey'>
							<Icon name='clock' /> 
							<Header.Content>
								Horários
							</Header.Content>
						</Header>
						<List>
							{ horarios && horarios.join(', ') }
						</List>
					</Grid.Column>

					<Grid.Column>
						<Header as='h3' color='grey'>
							<Icon name='treatment' /> 
							<Header.Content>
								Especializações
							</Header.Content>
						</Header>
						<List>
							{ specs }
						</List>
					</Grid.Column>
				</Grid.Row>
			</Grid>

			<Dimmer inverted active={this.props.medicos.fetching}>
				<Loader inverted>Carregando...</Loader>
			</Dimmer>
		</div>;
	}
}

const mapStateToProps = ( store ) => {
	return {
		medicos: store.medicos
	};
}

export default connect(mapStateToProps)(DetalhesComponent);