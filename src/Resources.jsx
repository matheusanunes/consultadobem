import axios from 'axios';

import Config from './Config';

/**
 * Factory for fesources for forms and all other info needed from the database
 */

class ResourcesClass {
 
 	// Define o objeto de funções publicas
 	factory = {};

	// Define as constantes necessarias para os requests
	resources = {
		connection 	: '', 
		base_url 	: '', 
		method 		: 'get', 
		headers 	: {}, 
	};

 	// Constroi os parametros padrão para o objeto
	// Construct
	constructor(){

		// URL base dos requests
		this.setBasePath( Config.get('server.base_url') );

		// Header padrão
		this.setHeader('Content-Type', 'application/json');

	}



	/**
	 * Cria o objeto base para o request
	 * @param  {string} resource Qual serviço a ser requerido
	 * @param  {object} data     Os dados do request
	 * @return {object}          O objeto base do request
	 */
	makeConnectionObject = ( resource, data, id ) => {
		return {
			method: this.resources.method.toUpperCase(),
			url	: this.getResourcePath(resource, data, id), 
			headers: this.resources.headers
		};
	}

	/**
	 * Retorna a url completa para o request
	 * @param  {string} type     Um alias do serviço
	 * @param  {object} resource Todos os dados da request, por exemplo o id de um item especifico
	 * @return {string}          A URL
	 */
	getResourcePath = ( type, resource, id ) => {
		var path = '';

		switch(type.toLowerCase()){

			case 'medicos':
				path += this.getApiPath();
				path += '/18qnux';
				break;



			// TESTA CONEXAO ////////
			default:
				path += this.getApiPath('login');
				break;
		}

		return path;
	}

	/**
	 * Retorna a URL da api 
	 * @return {string}     A URL
	 */
	getApiPath = ( ) => {
		let url = this.resources.base_url;
		return url;
	}

	/**
	 * Define o methodo da requisição. Ex.: GET, POST, PUT, DELETE, PATCH
	 * @param {string} method O metodo
	 */
	setMethod = ( method ) => {
		this.resources.method = method.toLowerCase();
	}

	/**
	 * Define a URL base para o request
	 * @param {string} url A url
	 */
	setBasePath = ( url ) => {
		this.resources.base_url = url;
	}

	/**
	 * Define ou sobreescreve headers para a requisição
	 * @param {string} header O header
	 * @param {string} type   Valor
	 */
	setHeader = ( header, type ) => {
		this.resources.headers[header] = type;
	}

	/**
	 * Define as variaveis de requisição
	 * @param  {object} data O objeto de requisição
	 * @return {object} 	 O objeto seguro de requisição
	 */
	setSecurityBinddings = ( request, data ) =>  {
		let obj = { ...request };
		
		// eslint-disable-next-line
		switch( this.resources.method.toLowerCase() ){
			case 'get':
			case 'delete':
					
					// Nada a fazer aqui

				break;

			case 'post':
			case 'put':

					// Colocamos os valores na request e retornamos
					obj.data = data;

				break;
		}

		return obj;
	}

	/**
	 * Define a estrutura de dados da requisição
	 * @param {object} data Os dados da requisição
	 */
	setData = ( request, data ) => {
		return this.setSecurityBinddings( request, data );
	}


	/**
	 * Executa o request
	 * @param  {object} obj O objeto final da requisição
	 * @return {object}     A promessa de requisição
	 */
	run = ( obj ) => {
		return 	axios(obj);
	}


	/**
	 * Executa a operação GET
	 * @param  {string} resource Qual o tipo de recurso
	 * @param  {object} data     O dados da requisição
	 * @return {object}          A promessa
	 */
	get = ( resource, data ) => {
		this.setMethod('get');
		let request = this.makeConnectionObject( resource, data );
			request = this.setData(request, data);

		return this.run(request);
	}

	save = ( resource, data ) => {
		this.setMethod('post');
		let request  = this.makeConnectionObject( resource, data );
		
		if( typeof data !== 'undefined'){
			request = this.setData(request, data);
		}

		return this.run(request);
	}

	update = ( resource, data, id ) => {
		this.setMethod('put');
		let request  = this.makeConnectionObject( resource, data, id );
		if( typeof data !== 'undefined'){
			request = this.setData(request, data);
		}
		return this.run(request);
	}
	

	delete = ( resource, data ) => {
		this.setMethod('delete');
		let request = this.makeConnectionObject( resource, data );
			request = this.setData(resource, data);

		return this.run(request);
	}
}

const Resources = new ResourcesClass();
export default Resources;