import React, { Component } from'react';
import { connect } from 'react-redux';
import Config from '../Config';

import { Icon, Input, Label, Menu } from 'semantic-ui-react';

import { searchTerm } from '../actions/searchActions';

const CidadeAtual = Config.get('cidade_padrao');

class MenuPrincipal extends Component {

	state = { value: '' };

	handleOnChange = ( e, {name, value} ) => {
		this.setState({ value });
		this.props.search( value )
	}

	render() {

		let loading = this.props.medicos.fetching,
			medicos = this.props.medicos.filtrados ? this.props.medicos.filtrados : this.props.medicos.medicos;

		const medic_count = this.props.medicos.fetched ? medicos.length : 0;
		const city_count  = this.props.medicos.fetched ? [...new Set( medicos.map( med => med.cidade_atendimento ) )].length : 0;
		const espc_count  = this.props.medicos.fetched ? medicos.reduce((valorAnterior, valorAtual, indice, array) => {
			return [ ...valorAnterior ].concat( valorAtual.especialidades.filter( espc => valorAnterior.indexOf(espc) === -1));
		}, []).length : 0;

		return <div id={this.constructor.name}>
			<Menu vertical fluid>
				<Menu.Item as={'div'}>
					<strong>Bem vindo(a) usuário(a)</strong><br /><br />
					Cidade atual: {CidadeAtual}
				</Menu.Item>

				<Menu.Item as='div'>
					<Label color='teal'>
						{loading ? <Icon name='spinner' loading /> : medic_count}
					</Label>
					Médicos
				</Menu.Item>

				<Menu.Item as='div'>
					<Label color='teal'>
						{loading ? <Icon name='spinner' loading /> : city_count}
					</Label>
					Cidades
				</Menu.Item>

				<Menu.Item as='div'>
					<Label color='teal'>
						{loading ? <Icon name='spinner' loading /> : espc_count}
					</Label>
					Especializações
				</Menu.Item>

				<Menu.Item>
					<Input icon='search' name='search' placeholder='Pesquisar médicos...' value={this.state.value} onChange={this.handleOnChange} />
				</Menu.Item>
			</Menu>
		</div>;
	}
}

const mapStateToProps = ( store ) => {
	return {
		medicos: store.medicos
	};
}

const mapDispatchToProps = ( dispatch ) => {
	return {
		search: ( term ) => { dispatch(searchTerm(term)); }
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuPrincipal);